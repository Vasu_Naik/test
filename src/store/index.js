import { reactive } from 'vue';

const state = reactive({
  cart: [],
});

const addToCart = (product) => {
  state.cart.push(product);
};

const store = {
  state,
  addToCart,
};

export default store;
